IN=interp.c
OUT=pain
INSTLOC=/usr/bin

$(OUT):
	$(CC) -ansi -Wall -Wextra -pedantic -Werror $(IN) -o $(OUT)
install:
	mv $(OUT) $(INSTLOC)

# PAINLANG
A programming language designed to maximise pain
# USAGE

Painlang has 5 commands.

* i

i moves the pointer forwards and increments your present cell
* d

if the present value of your cell is not 0, d moves back the pointer and decrements the present cell. otherwise, d moves the pointer back
* n

n moves the pointer forwards
* p

p prints the ascii value of the number in the current cell
* f

f asks for input and stores it as a number
* x

x adds [previous cell val] amount of times and then moves forward [previous cell val mod 2] amount of times

* a

a adds the current cell's value and the next cell's value and puts it inside the current cell

If a command other than one of these commands is found, painlang will put you on a random cell

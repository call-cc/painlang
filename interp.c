#include <stdio.h>
#include <stdlib.h>
#define TAPESIZE 3000

int main(int argc, char *argv[]) {
  int i;
  int loop;
  char c = 'a';
  int tape[TAPESIZE];
  int pointer = 0;
  FILE* file;
  if(argc == 1) {
    fprintf(stderr, "Usage: pain <file>\n");
    exit(1);
  } else {
    file = fopen(argv[1], "r");
    if(file == NULL) {
      fprintf(stderr, "Error opening file\n");
      exit(1);
    }
    while(c != EOF) {
      c = fgetc(file);
      if(c == 'i') { tape[pointer]++; pointer++; }
      else if(c == '!') printf("%d %d\n", pointer, tape[pointer]);
      else if(c == 'd') {
        if(tape[pointer] != 0) { tape[pointer]--; pointer--; }
        else pointer--;
      }
      else if(c == 'n') pointer++;
      else if(c == 'p') printf("%c", tape[pointer]);
      else if(c == 'f') tape[pointer] = getchar();
      else if(c == 'x') {
	for(i = 0; i <= tape[pointer - 1]; i++) {
	  tape[pointer]++;
	}
	for(loop = 0; loop <= (tape[pointer] % 2); loop++) {
	  pointer++;
	}
      }
      else if(c == 'a') {
	if(pointer + 1 > TAPESIZE - 1) {
	  fprintf(stderr, "Reached the end of the tape!\n");
	  exit(1);
	} else {
	  tape[pointer] += tape[pointer + 1];
	}
      }
      else if(c == '\n') { }
      else {
        pointer = rand() & (TAPESIZE - 1);
      }
    }
  }
  fclose(file);
}

	   
	   

